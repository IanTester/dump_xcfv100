/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <filesystem>
#include <vector>
#include <unordered_map>
#include <memory>
#include <string>
#include <stdint.h>
#include <yaml-cpp/yaml.h>

#include "xcf_enums.hh"

namespace fs = std::filesystem;

namespace XCF {

  //! Role class for sections of a stream
  class stream_section_role {
  protected:
    std::istream &_stream;
    uint32_t _section_offset;

    stream_section_role(std::istream& s, uint32_t o);

    void _seek_section(uint32_t additional_offset = 0);

  }; // class stream_section_role


  using PropValue = std::vector<uint8_t>;
  using PropList = std::unordered_map<PropType, PropValue>;

  class Colour;

  struct RGB8 {
    uint8_t red, green, blue;
  };

  YAML::Emitter& operator <<(YAML::Emitter& out, const RGB8& col);

  enum class guide_orientation : uint8_t {
    horizontal	= 1,
    vertical	= 2,
  };

  struct guide {
    int32_t position;
    int8_t orientation;
  };

  YAML::Emitter& operator <<(YAML::Emitter& out, const guide& g);

  //! Role class for loading and storing properties
  class properties_role {
  protected:
    PropList _properties;

    void _load_properties(std::istream& is);

  public:
    bool has_property(PropType type) const;

    const PropValue& property(PropType type) const;

    const std::vector<RGB8> get_colormap(void) const;
    const float get_opacity(void) const;
    const int32_t get_mode(void) const;
    const bool get_visible(void) const;
    const bool get_linked(void) const;
    const bool get_preserve_transparency(void) const;
    const bool get_apply_mask(void) const;
    const bool get_edit_mask(void) const;
    const bool get_show_mask(void) const;
    const bool get_show_masked(void) const;
    const std::pair<int32_t, int32_t> get_offsets(void) const;
    const Colour get_colour(void) const;
    const uint8_t get_compression(void) const;
    const std::vector<guide> get_guides(void) const;

  }; // class properties_role


  //! Role template class for loading and storing colour "tag" information
  template <typename P>
  class tag_role {
  protected:
    uint8_t _type, _alpha, _format, _precision;

    //! Private empty constructor
    tag_role();

    //! Private constructor from elements
    tag_role(uint8_t t, uint8_t a, uint8_t f, uint8_t p);

    virtual ~tag_role() {}

    void _load_tag(std::istream& is);

    uint32_t _num_channels(void) const;
    uint32_t _bytes_per_channel(void) const;

  }; // class tag_role


  class Hierarchy;
  class Layer;
  class File;


  class Image : private tag_role<Image> {
  private:
    uint32_t _width, _height;
    uint32_t _pixel_size;
    std::vector<uint8_t*> _rows;

  public:
    using ptr = std::shared_ptr<Image>;

    //! Constructor
    Image(uint32_t w, uint32_t h, uint8_t t, uint8_t a, uint8_t f, uint8_t p);

    ~Image();

    void insert_row(uint32_t x, uint32_t y, uint32_t width, const std::vector<uint8_t>& row);

    void save_png(fs::path filepath, const std::shared_ptr<File> xcf_file, const properties_role& owner, fs::file_time_type mtime);

  }; // class Image


  class Row : private tag_role<Row> {
  private:
    uint32_t _width;
    std::vector<uint8_t> _data;

    //! Private constructor from stream
    Row(std::istream& is);

    uint32_t width(void) const;

    friend class Hierarchy;

  }; // class Row


  class Hierarchy : private stream_section_role, private tag_role<Hierarchy> {
  private:
    uint32_t _width, _height;
    uint32_t _bytes, _storage, _autoalloc;

    //! Private constructor from stream
    Hierarchy(std::istream& is, uint32_t offset);

    Image::ptr _load(void);

    uint32_t width(void) const;
    uint32_t height(void) const;
    uint32_t bytes(void) const;
    StorageType storage(void) const;
    AutoAlloc autoalloc(void) const;

    using ptr = std::shared_ptr<Hierarchy>;

    friend class Layer;
    friend class Channel;

  }; // class Hierarchy


  class Colour : private tag_role<Colour> {
  public:
    //! Constructor from 32-bit value
    Colour(uint32_t tag);

    const std::string type_str(void) const;
    const std::string alpha_str(void) const;
    const std::string format_str(void) const;
    const std::string precision_str(void) const;

  }; // class Colour

  YAML::Emitter& operator <<(YAML::Emitter& out, const Colour& c);


  class Channel : private stream_section_role, public properties_role {
  private:
    uint32_t _width, _height;
    std::string _name;
    Hierarchy::ptr _hierarchy;

    //! Private constructor from stream
    Channel(std::istream& is, uint32_t offset);

    friend class Layer;
    friend class File;

  public:
    using ptr = std::shared_ptr<Channel>;

    uint32_t width(void) const;
    uint32_t height(void) const;
    std::string name(void) const;

    Image::ptr image(void) const;

  }; // class Channel

  YAML::Emitter& operator <<(YAML::Emitter& out, const Channel& c);


  class Layer : private stream_section_role, public properties_role, private tag_role<Layer> {
  private:
    uint32_t _width, _height;
    std::string _name;
    Hierarchy::ptr _hierarchy;
    Channel::ptr _mask;

    //! Private constructor from stream
    Layer(std::istream& is, uint32_t offset);

    friend class File;

  public:
    using ptr = std::shared_ptr<Layer>;

    uint32_t width(void) const;
    uint32_t height(void) const;
    std::string name(void) const;

    Image::ptr image(void) const;

    bool has_mask(void) const;

    std::string mask_name(void) const;
    Channel::ptr mask(void) const;
    Image::ptr mask_image(void) const;

  }; // class Layer

  YAML::Emitter& operator <<(YAML::Emitter& out, const Layer& l);


  class File : public std::enable_shared_from_this<File>, public properties_role, private tag_role<File> {
  private:
    uint32_t _width, _height;
    std::vector<Layer> _layers;
    std::vector<Channel> _channels;

    //! Private constructor from stream
    File(std::istream& is);

  public:
    using ptr = std::shared_ptr<File>;

    uint32_t width(void) const;
    uint32_t height(void) const;

    uint32_t num_layers(void) const;
    uint32_t num_channels(void) const;

    const Layer& layer(uint32_t num) const;
    const Channel& channel(uint32_t num) const;

    //! Class method to load a file from a stream
    static ptr load(std::istream& is);

  }; // class File

  YAML::Emitter& operator <<(YAML::Emitter& out, const File& f);


}; // namespace XCF

#include "xcf.tcc"
