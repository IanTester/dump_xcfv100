/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "xcf.hh"
#include <iostream>
#include <exception>
#include <string>
#include <cstring>

#include "be.hh"

namespace XCF {

  std::string read_string(std::istream& is) {
    uint32_t len = read_be<uint32_t>(is);
    char bytes[len];
    is.read(bytes, len);
    return std::string(bytes, len - 1);
  }


  stream_section_role::stream_section_role(std::istream& s, uint32_t o) :
    _stream(s),
    _section_offset(o)
  {}

  void stream_section_role::_seek_section(uint32_t additional_offset) {
    _stream.seekg(_section_offset + additional_offset);
  }


  YAML::Emitter& operator <<(YAML::Emitter& out, const RGB8& col) {
    out << YAML::Flow << YAML::BeginSeq
	<< col.red << col.green << col.blue
	<< YAML::EndSeq;

    return out;
  }

  YAML::Emitter& operator <<(YAML::Emitter& out, const guide& g) {
    out << YAML::BeginMap << YAML::Flow;

    out << YAML::Key << "position"
	<< YAML::Value << g.position;
    out << YAML::Key << "orientation"
	<< YAML::Value;
    switch (g.orientation) {
    case static_cast<int8_t>(guide_orientation::horizontal):
      out << "horizontal";
      break;

    case static_cast<int8_t>(guide_orientation::vertical):
      out << "vertical";
      break;

    default:
      out << "unknown";
      break;
    }

    out << YAML::EndMap;
    return out;
  }


  void properties_role::_load_properties(std::istream& is) {
    uint32_t proptype = read_be<uint32_t>(is);
    uint32_t propsize = read_be<uint32_t>(is);
    while (proptype != 0) {
      if (proptype < static_cast<uint8_t>(PropType::SPLINES_START)) {
	PropValue data(propsize);
	is.read(reinterpret_cast<char*>(data.data()), propsize);
	_properties.emplace(static_cast<PropType>(proptype), data);
      } else
	is.ignore(propsize);

      proptype = read_be<uint32_t>(is);
      propsize = read_be<uint32_t>(is);
    }
  }

  bool properties_role::has_property(PropType type) const {
    return _properties.contains(type);
  }

  const PropValue& properties_role::property(PropType type) const {
    return _properties.at(type);
  }

  const std::vector<RGB8> properties_role::get_colormap(void) const {
    auto prop = _properties.at(PropType::COLORMAP);
    uint32_t pos = 0;
    auto num_cols = read_be<uint32_t>(prop, pos);
    pos += 4;

    std::vector<RGB8> colormap(num_cols);
    for (uint32_t i = 0; i < num_cols; i++) {
      auto r = read_be<uint8_t>(prop, pos);
      auto g = read_be<uint8_t>(prop, pos + 1);
      auto b = read_be<uint8_t>(prop, pos + 2);
      pos += 3;

      colormap.push_back(RGB8(r, g, b));
    }

    return colormap;
  }

  const float properties_role::get_opacity(void) const {
    return read_be<float>(_properties.at(PropType::OPACITY), 0);
  }

  const int32_t properties_role::get_mode(void) const {
    return read_be<int32_t>(_properties.at(PropType::MODE), 0);
  }

  const bool properties_role::get_visible(void) const {
    return read_be<uint32_t>(_properties.at(PropType::VISIBLE), 0);
  }

  const bool properties_role::get_linked(void) const {
    return read_be<uint32_t>(_properties.at(PropType::LINKED), 0);
  }

  const bool properties_role::get_preserve_transparency(void) const {
    return read_be<uint32_t>(_properties.at(PropType::PRESERVE_TRANSPARENCY), 0);
  }

  const bool properties_role::get_apply_mask(void) const {
    return read_be<uint32_t>(_properties.at(PropType::APPLY_MASK), 0);
  }

  const bool properties_role::get_edit_mask(void) const {
    return read_be<uint32_t>(_properties.at(PropType::EDIT_MASK), 0);
  }

  const bool properties_role::get_show_mask(void) const {
    return read_be<uint32_t>(_properties.at(PropType::SHOW_MASK), 0);
  }

  const bool properties_role::get_show_masked(void) const {
    return read_be<uint32_t>(_properties.at(PropType::SHOW_MASKED), 0);
  }

  const std::pair<int32_t, int32_t> properties_role::get_offsets(void) const {
    auto bytes = _properties.at(PropType::OFFSETS);
    return std::make_pair<int32_t, int32_t>(read_be<int32_t>(bytes, 0),
					    read_be<int32_t>(bytes, 4));
  }

  const Colour properties_role::get_colour(void) const {
    return Colour(read_be<int32_t>(_properties.at(PropType::COLOR), 0));
  }

  const uint8_t properties_role::get_compression(void) const {
    return read_be<uint8_t>(_properties.at(PropType::COMPRESSION), 0);
  }

  const std::vector<guide> properties_role::get_guides(void) const {
    auto prop = _properties.at(PropType::GUIDES);
    uint32_t num_guides = prop.size() / sizeof(guide);

    std::vector<guide> guides(num_guides);
    uint32_t pos = 0;
    for (uint32_t i = 0; i < num_guides; i++) {
      auto position = read_be<int32_t>(prop, pos);
      pos += 4;
      auto orientation = read_be<int8_t>(prop, pos);
      pos++;

      guides.push_back(guide(position, orientation));
    }

    return guides;
  }


  Image::Image(uint32_t w, uint32_t h, uint8_t t, uint8_t a, uint8_t f, uint8_t p) :
    tag_role(t, a, f, p),
    _width(w), _height(h),
    _pixel_size(_num_channels() * _bytes_per_channel()),
    _rows(_height)
  {
    uint32_t rowsize = _width * _pixel_size;
    for (uint32_t y = 0; y < _height; y++)
      _rows[y] = new uint8_t[rowsize];
  }

  Image::~Image() {
    for (auto row : _rows)
      delete [] row;
  }

  void Image::insert_row(uint32_t x, uint32_t y, uint32_t width, const std::vector<uint8_t>& row) {
    std::memcpy(_rows[y] + (x * _pixel_size), row.data(), width * _pixel_size);
  }


  Row::Row(std::istream& is) :
    tag_role()
  {
    _load_tag(is);
    _width = read_be<uint32_t>(is);
    uint32_t len = _width * _num_channels() * _bytes_per_channel();
    _data.resize(len);
    is.read(reinterpret_cast<char*>(_data.data()), len);
  }

  uint32_t Row::width(void) const {
    return _width;
  }


  Hierarchy::Hierarchy(std::istream& is, uint32_t offset) :
    stream_section_role(is, offset),
    tag_role()
  {
    _seek_section();
    _width = read_be<uint32_t>(is);
    _height = read_be<uint32_t>(is);
    _load_tag(is);
    _bytes = read_be<uint32_t>(is);
    _storage = read_be<uint32_t>(is);
    _autoalloc = read_be<uint32_t>(is);
  }

  Image::ptr Hierarchy::_load(void) {
    auto image = std::make_shared<Image>(_width, _height, _type, _alpha, _format, _precision);

    _seek_section(6 * 4);
    for (uint32_t y = 0; y < _height; y += 128) {
      uint32_t th = 128;
      if (y + 128 >= _height)
	th = _height - y;

      for (uint32_t x = 0; x < _width; x += 128) {
	for (uint32_t ty = th; ty; ty--) {
	  Row row(_stream);
	  image->insert_row(x, y + ty - 1, row._width, row._data);
	}
      }
    }

    return image;
  }

  uint32_t Hierarchy::width(void) const {
    return _width;
  }

  uint32_t Hierarchy::height(void) const {
    return _height;
  }

  uint32_t Hierarchy::bytes(void) const {
    return _bytes;
  }

  StorageType Hierarchy::storage(void) const {
    return static_cast<StorageType>(_storage);
  }

  AutoAlloc Hierarchy::autoalloc(void) const {
    return static_cast<AutoAlloc>(_autoalloc);
  }


  Colour::Colour(uint32_t tag) :
    tag_role(tag & 0xff,
	     (tag >> 8) & 0xff,
	     (tag >> 16) & 0xff,
	     tag >> 24)
  {}

  const std::string Colour::type_str(void) const {
    return ImageType_names[_type];
  }

  const std::string Colour::alpha_str(void) const {
    return ImageAlpha_names[_alpha];
  }

  const std::string Colour::format_str(void) const {
    return ImageFormat_names[_format];
  }

  const std::string Colour::precision_str(void) const {
    return ImagePrecision_names[_precision];
  }

  YAML::Emitter& operator <<(YAML::Emitter& out, const Colour& c) {
    out << YAML::BeginMap;
    out << YAML::Key << "type" << YAML::Value << c.type_str();
    out << YAML::Key << "alpha" << YAML::Value << c.alpha_str();
    out << YAML::Key << "format" << YAML::Value << c.format_str();
    out << YAML::Key << "precision" << YAML::Value << c.precision_str();
    out << YAML::EndMap;
    return out;
  }


  Channel::Channel(std::istream& is, uint32_t offset) :
    stream_section_role(is, offset)
  {
    _seek_section();
    _width = read_be<uint32_t>(is);
    _height = read_be<uint32_t>(is);
    _name = read_string(is);

    std::cerr << "\tread channel header: width=" << _width
	      << ", height=" << _height
	      << ", name=\"" << _name << "\""
	      << "." << std::endl;

    _load_properties(is);

    uint32_t hierarchy_offset = read_be<uint32_t>(is);
    if (hierarchy_offset != 0) {
      auto h = new Hierarchy(is, hierarchy_offset);
      _hierarchy = Hierarchy::ptr(h);
    }
  }

  uint32_t Channel::width(void) const {
    return _width;
  }

  uint32_t Channel::height(void) const {
    return _height;
  }

  std::string Channel::name(void) const {
    return _name;
  }

  Image::ptr Channel::image(void) const {
    return _hierarchy->_load();
  }

  YAML::Emitter& operator <<(YAML::Emitter& out, const Channel& c) {
    out << YAML::BeginMap;

    out << YAML::Key << "width" << YAML::Value << c.width();
    out << YAML::Key << "height" << YAML::Value << c.height();
    out << YAML::Key << "name" << YAML::Value << c.name();

    out << YAML::Key << "properties"
	<< YAML::Value << YAML::BeginMap;

    out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::ACTIVE_CHANNEL)]
	<< YAML::Value << c.has_property(PropType::ACTIVE_CHANNEL);

    out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::SELECTION)]
	<< YAML::Value << c.has_property(PropType::SELECTION);

    if (c.has_property(PropType::OPACITY)) {
      auto opa = c.get_opacity() * 100.0;
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::OPACITY)]
	  << YAML::Value << opa;
    }

    if (c.has_property(PropType::VISIBLE)) {
      auto vis = c.get_visible();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::VISIBLE)]
	  << YAML::Value << vis;
    }

    if (c.has_property(PropType::SHOW_MASKED)) {
      auto sm = c.get_show_masked();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::SHOW_MASKED)]
	  << YAML::Value << sm;
    }

    if (c.has_property(PropType::COLOR)) {
      auto col = c.get_colour();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::COLOR)]
	  << YAML::Value << col;
    }

    out << YAML::EndMap; // properties

    out << YAML::EndMap;
    return out;
  }


  Layer::Layer(std::istream& is, uint32_t offset) :
    stream_section_role(is, offset),
    tag_role()
  {
    _seek_section();
    _width = read_be<uint32_t>(is);
    _height = read_be<uint32_t>(is);
    _load_tag(is);
    _name = read_string(is);

    std::cerr << "\tread layer header: width=" << _width
	      << ", height=" << _height
	      << ", type=" << ImageType_names[_type]
	      << ", alpha=" << ImageAlpha_names[_alpha]
	      << ", format=" << ImageFormat_names[_format]
	      << ", precision=" << ImagePrecision_names[_precision]
	      << ", name=\"" << _name << "\""
	      << "." << std::endl;

    _load_properties(is);

    uint32_t hierarchy_offset = read_be<uint32_t>(is);
    uint32_t mask_offset = read_be<uint32_t>(is);

    if (hierarchy_offset != 0) {
      auto h = new Hierarchy(is, hierarchy_offset);
      _hierarchy = Hierarchy::ptr(h);
    }

    if (mask_offset != 0) {
      auto m = new Channel(is, mask_offset);
      _mask = Channel::ptr(m);
    }
  }

  uint32_t Layer::width(void) const {
    return _width;
  }

  uint32_t Layer::height(void) const {
    return _height;
  }

  std::string Layer::name(void) const {
    return _name;
  }

  Image::ptr Layer::image(void) const {
    return _hierarchy->_load();
  }

  bool Layer::has_mask(void) const {
    if (_mask)
      return true;
    return false;
  }

  std::string Layer::mask_name(void) const {
    if (_mask)
      return _mask->_name;
    return std::string();
  }

  Channel::ptr Layer::mask(void) const {
    return _mask;
  }

  Image::ptr Layer::mask_image(void) const {
    if (_mask)
      return _mask->_hierarchy->_load();

    return nullptr;
  }

  YAML::Emitter& operator <<(YAML::Emitter& out, const Layer& l) {
    out << YAML::BeginMap;

    out << YAML::Key << "width" << YAML::Value << l.width();
    out << YAML::Key << "height" << YAML::Value << l.height();
    out << YAML::Key << "name" << YAML::Value << l.name();

    out << YAML::Key << "properties"
	<< YAML::Value << YAML::BeginMap;

    out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::ACTIVE_LAYER)]
	<< YAML::Value << l.has_property(PropType::ACTIVE_LAYER);

    out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::FLOATING_SELECTION)]
	<< YAML::Value << l.has_property(PropType::FLOATING_SELECTION);

    if (l.has_property(PropType::OPACITY)) {
      auto opa = l.get_opacity() * 100.0;
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::OPACITY)]
	  << YAML::Value << opa;
    }

    if (l.has_property(PropType::VISIBLE)) {
      auto vis = l.get_visible();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::VISIBLE)]
	  << YAML::Value << vis;
    }

    if (l.has_property(PropType::LINKED)) {
      auto linked = l.get_linked();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::LINKED)]
	  << YAML::Value << linked;
    }

    if (l.has_property(PropType::PRESERVE_TRANSPARENCY)) {
      auto pt = l.get_preserve_transparency();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::PRESERVE_TRANSPARENCY)]
	  << YAML::Value << pt;
    }

    if (l.has_property(PropType::APPLY_MASK)) {
      auto am = l.get_apply_mask();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::APPLY_MASK)]
	  << YAML::Value << am;
    }

    if (l.has_property(PropType::EDIT_MASK)) {
      auto em = l.get_edit_mask();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::EDIT_MASK)]
	  << YAML::Value << em;
    }

    if (l.has_property(PropType::SHOW_MASK)) {
      auto sm = l.get_show_mask();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::SHOW_MASK)]
	  << YAML::Value << sm;
    }

    if (l.has_property(PropType::OFFSETS)) {
      auto offsets = l.get_offsets();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::OFFSETS)]
	  << YAML::Value
	  << YAML::Flow << YAML::BeginSeq << offsets.first << offsets.second << YAML::EndSeq;
    }

    if (l.has_property(PropType::MODE)) {
      auto m = l.get_mode();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::MODE)]
	  << YAML::Value << Mode_names[m];
    }

    // TODO: Splines

    out << YAML::EndMap; // properties

    if (l.has_mask()) {
      out << YAML::Key << "mask"
	  << YAML::Value << *(l.mask());
    }

    out << YAML::EndMap;
    return out;
  }


  File::File(std::istream& is) :
    tag_role(),
    _width(0), _height(0)
  {
    // Read file header
    _width = read_be<uint32_t>(is);
    _height = read_be<uint32_t>(is);
    _load_tag(is);
    std::cerr << "read header: width=" << width()
	      << ", height=" << height()
	      << ", type=" << ImageType_names[_type]
	      << ", alpha=" << ImageAlpha_names[_alpha]
	      << ", format=" << ImageFormat_names[_format]
	      << ", precision=" << ImagePrecision_names[_precision]
	      << "." << std::endl;

    _load_properties(is);

    std::vector<uint32_t> layer_offsets, channel_offsets;
    {
      // Read layer offsets
      uint32_t offset = read_be<uint32_t>(is);
      std::cerr << "read layer offset " << offset << "." << std::endl;
      while (offset != 0) {
	layer_offsets.push_back(offset);

	offset = read_be<uint32_t>(is);
	std::cerr << "read layer offset " << offset << "." << std::endl;
      }
      std::cerr << "Got " << layer_offsets.size() << " layer offsets." << std::endl;

      // Read channel offsets
      offset = read_be<uint32_t>(is);
      std::cerr << "read channel offset " << offset << "." << std::endl;
      while (offset != 0) {
	channel_offsets.push_back(offset);

	offset = read_be<uint32_t>(is);
	std::cerr << "read channel offset " << offset << "." << std::endl;
      }
      std::cerr << "Got " << channel_offsets.size() << " channel offsets." << std::endl;
    }

    for (auto offset : layer_offsets)
      _layers.push_back(Layer(is, offset));

    for (auto offset : channel_offsets)
      _channels.push_back(Channel(is, offset));
  }

  uint32_t File::width(void) const {
    return _width;
  }

  uint32_t File::height(void) const {
    return _height;
  }

  uint32_t File::num_layers(void) const {
    return _layers.size();
  }

  uint32_t File::num_channels(void) const {
    return _channels.size();
  }

  const Layer& File::layer(uint32_t num) const {
    return _layers.at(num);
  }

  const Channel& File::channel(uint32_t num) const {
    return _channels.at(num);
  }

  File::ptr File::load(std::istream& is) {
    {
      std::string signature;
      std::getline(is, signature, '\0');
      if (signature != "gimp xcf v100") {
	std::cerr << "File signature (\"" << signature << "\") is incorrect." << std::endl;
	return nullptr;
      }
    }

    auto f = new File(is);
    return File::ptr(f);
  }

  YAML::Emitter& operator <<(YAML::Emitter& out, const File& f) {
    out << YAML::BeginMap;

    out << YAML::Key << "width" << YAML::Value << f.width();
    out << YAML::Key << "height" << YAML::Value << f.height();

    out << YAML::Key << "properties"
	<< YAML::Value << YAML::BeginMap;

    if (f.has_property(PropType::COLORMAP)) {
      auto colormap = f.get_colormap();

      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::COLORMAP)]
	  << YAML::Value << colormap;
    }

    if (f.has_property(PropType::COMPRESSION)) {
      auto comp = f.get_compression();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::COMPRESSION)]
	  << YAML::Value << CompressionType_names[comp];
    }

    if (f.has_property(PropType::GUIDES)) {
      auto guides = f.get_guides();
      out << YAML::Key << PropType_names[static_cast<uint8_t>(PropType::GUIDES)]
	  << YAML::Value << guides;
    }

    out << YAML::EndMap; // properties

    out << YAML::Key << "layers" << YAML::Value;
    out << YAML::BeginSeq;
    for (uint32_t layer_num = 0; layer_num < f.num_layers(); layer_num++) {
      auto layer = f.layer(layer_num);
      out << layer;
    }
    out << YAML::EndSeq;

    out << YAML::Key << "channels" << YAML::Value;
    out << YAML::BeginSeq;
    for (uint32_t channel_num = 0; channel_num <f.num_channels(); channel_num++) {
      auto channel = f.channel(channel_num);
      out << channel;
    }
    out << YAML::EndSeq;

    out << YAML::EndMap;
    return out;
  }


}; // namespace XCF
