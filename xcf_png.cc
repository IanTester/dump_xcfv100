/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "xcf.hh"

#include <iostream>
#include <fstream>
#include <fmt/core.h>
#include <png.h>
#include <zlib.h>

namespace XCF {

  //! libPNG callback for writing to an ostream
  void png_write_ostream_cb(png_structp png, png_bytep buffer, png_size_t length) {
    std::ostream *os = (std::ostream*)png_get_io_ptr(png);
    os->write((char*)buffer, length);
  }

  //! libPNG callback for flushing an ostream
  void png_flush_ostream_cb(png_structp png) {
    std::ostream *os = (std::ostream*)png_get_io_ptr(png);
    os->flush();
  }


  void Image::save_png(fs::path filepath, const std::shared_ptr<File> xcf_file, const properties_role& owner, fs::file_time_type mtime) {
    std::ofstream ofs;
    ofs.open(filepath, std::ios_base::out);

    png_structp png = png_create_write_struct(PNG_LIBPNG_VER_STRING,
                                              nullptr, nullptr, nullptr);
    if (!png) {
      std::cerr << "Could not create PNG write structure." << std::endl;
      return;
    }

    png_infop info = png_create_info_struct(png);
    if (!info) {
      std::cerr << "Could not create PNG info structure." << std::endl;
      return;
    }

    if (setjmp(png_jmpbuf(png))) {
      png_destroy_write_struct(&png, &info);
      ofs.close();
      std::cerr << "Something went wrong writing the PNG." << std::endl;
      return;
    }

    png_set_write_fn(png, &ofs, png_write_ostream_cb, png_flush_ostream_cb);

    int png_colour_type;
    switch (static_cast<ImageFormat>(_format)) {
    case ImageFormat::RGB:
      png_colour_type = PNG_COLOR_TYPE_RGB;
      break;

    case ImageFormat::GREY:
      png_colour_type = PNG_COLOR_TYPE_GRAY;
      break;

    case ImageFormat::INDEXED:
      png_colour_type = PNG_COLOR_TYPE_PALETTE;
      break;

    case ImageFormat::NONE:
    default:
      std::cerr << "Image format is none." << std::endl;
      return;
    }

    if (_alpha == static_cast<uint8_t>(ImageAlpha::YES))
      png_colour_type |= PNG_COLOR_MASK_ALPHA;

    png_set_IHDR(png, info,
                 _width, _height, _bytes_per_channel() << 3, png_colour_type,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_set_filter(png, 0, PNG_ALL_FILTERS);
    png_set_compression_level(png, Z_BEST_COMPRESSION);

    if (_format == static_cast<uint8_t>(ImageFormat::INDEXED)) {
      if (!xcf_file->has_property(PropType::COLORMAP)) {
	std::cerr << "Layer is indexed but XCF file does not have COLORMAP property." << std::endl;
	return;
      }

      auto colourmap = xcf_file->get_colormap();
      if (colourmap.size() > PNG_MAX_PALETTE_LENGTH) {
	std::cerr << "COLORMAP has more than " << PNG_MAX_PALETTE_LENGTH
		  << " entries." << std::endl;
	return;
      }

      png_color palette[colourmap.size()];
      for (uint8_t i = 0; i < colourmap.size(); i++) {
	palette[i].red = colourmap[i].red;
	palette[i].green = colourmap[i].green;
	palette[i].blue = colourmap[i].blue;
      }

      png_set_PLTE(png, info, palette, colourmap.size());
    }

    // TODO: ICC profile?

    std::string comment_text;

    comment_text += "Active layer=" + std::string(owner.has_property(PropType::ACTIVE_LAYER) ? "true" : "false");

    // Channel prop
    comment_text += "\nActive channel=" + std::string(owner.has_property(PropType::ACTIVE_CHANNEL) ? "true" : "false");

    // Channel prop
    comment_text += "\nSelection=" + std::string(owner.has_property(PropType::SELECTION) ? "true" : "false");

    // Layer prop
    comment_text += "\nFloating selection=" + std::string(owner.has_property(PropType::FLOATING_SELECTION) ? "true" : "false");

    if (owner.has_property(PropType::OPACITY)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Opacity=" + fmt::format("{:.1f}%", owner.get_opacity() * 100);
    }

    // Layer prop
    if (owner.has_property(PropType::MODE)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Mode=" + Mode_names[owner.get_mode()];
    }

    if (owner.has_property(PropType::VISIBLE)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Visible=" + std::string(owner.get_visible() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::LINKED)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Linked=" + std::string(owner.get_linked() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::PRESERVE_TRANSPARENCY)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Preserve transparency=" + std::string(owner.get_preserve_transparency() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::APPLY_MASK)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Apply mask=" + std::string(owner.get_apply_mask() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::EDIT_MASK)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Edit mask=" + std::string(owner.get_edit_mask() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::SHOW_MASK)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Show mask=" + std::string(owner.get_show_mask() ? "true" : "false");
    }

    // Channel prop
    if (owner.has_property(PropType::SHOW_MASKED)) {
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Show masked=" + std::string(owner.get_show_masked() ? "true" : "false");
    }

    // Layer prop
    if (owner.has_property(PropType::OFFSETS)) {
      auto offsets = owner.get_offsets();
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Offset=(" + std::to_string(offsets.first) + ", " + std::to_string(offsets.second) + ")";
    }

    // Channel prop
    if (owner.has_property(PropType::COLOR)) {
      auto colour = owner.get_colour();
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Colour type=" + colour.type_str()
	+ ", alpha=" + colour.alpha_str()
	+ ", format=" + colour.format_str()
	+ ", precision=" + colour.precision_str();
    }

    if (xcf_file->has_property(PropType::GUIDES)) {
      auto guides = xcf_file->get_guides();
      if (comment_text.length() > 0)
	comment_text += "\n";
      comment_text += "Guides=[";
      bool first = true;
      for (auto guide : guides) {
	if (!first)
	  comment_text += ", ";
	first = false;

	switch (guide.orientation) {
	case static_cast<uint8_t>(guide_orientation::horizontal):
	  comment_text += "y=";
	  break;

	case static_cast<uint8_t>(guide_orientation::vertical):
	  comment_text += "x=";
	  break;

	default:
	  continue;
	}
	comment_text += std::to_string(guide.position);
      }
      comment_text += "]";
    }

    if (comment_text.length() > 0) {
      std::string key = "Comment";
      comment_text = "XCF v100 properties\n\n" + comment_text;
      png_text text;
      text.compression = PNG_TEXT_COMPRESSION_NONE;
      text.key = const_cast<char*>(key.c_str());
      text.text = const_cast<char*>(comment_text.c_str());
      text.text_length = comment_text.length();
      text.lang_key = nullptr;

      std::cerr << "\tAdding comment text chunk." << std::endl;
      png_set_text(png, info, &text, 1);
    }

    {
      std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::file_clock::to_sys(mtime));
      if (t > 0) {
	png_time ptime;
	png_convert_from_time_t(&ptime, t);
	std::cerr << "\tAdding time chunk." << std::endl;
	png_set_tIME(png, info, &ptime);
      }
    }

    png_write_info(png, info);

    for (uint32_t y = 0; y < _height; y++) {
      png_write_row(png, _rows[y]);
      std::cerr << "\r\tWritten " << y + 1 << " of " << _height << " rows";
    }
    std::cerr << "\r\tWritten " << _height << " of " << _height << " rows." << std::endl;

    png_write_end(png, info);

    png_destroy_write_struct(&png, &info);
    ofs.close();
  }

}; // namespace XCF
