/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "xcf_enums.hh"

namespace XCF {

  const std::string PropType_names[] = {
    "end",
    "colourmap",
    "active_layer",
    "active_channel",
    "selection",
    "floating_selection",
    "opacity",
    "mode",
    "visible",
    "linked",
    "preserve_transparency",
    "apply_mask",
    "edit_mask",
    "show_mask",
    "show_masked",
    "offsets",
    "colour",
    "compression",
    "guides",

    "splines_start",
    "splines_continuity",
    "splines_keyframe",
    "splines_active",
    "splines_type",
    "splines_draw_points",
    "splines_points_start",
    "splines_save_point",
    "splines_points_end",
    "spines_end"
  };

  const std::string CompressionType_names[] = { "none", "RLE", "zlib",
						"fractal" };
  const std::string ImageType_names[] = { "RGB", "grey", "indexes" };
  const std::string ImageAlpha_names[] = { "none", "no", "yes" };
  const std::string ImageFormat_names[] = { "none", "RGB", "grey",
					    "indexes" };
  const std::string ImagePrecision_names[] = { "none", "u8", "u16", "float",
					       "float16", "BFP" };

  const std::string StorageType_names[] = { "none", "flat", "tiled", "shm" };
  const std::string AutoAlloc_names[] = { "none", "off", "on" };

  const std::string Mode_names[] = { "normal", "dissolve", "behind",
				     "multiply", "screen", "overlay",
				     "difference", "addition", "subtract",
				     "darken only", "lighten only", "hue",
				     "saturation", "colour", "value", "erase"
				     "replace" };

}; // namespace XCF
