/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <string>

namespace XCF {

  enum class PropType : uint8_t {
    END				= 0,
    COLORMAP			= 1,
    ACTIVE_LAYER		= 2,
    ACTIVE_CHANNEL		= 3,
    SELECTION			= 4,
    FLOATING_SELECTION		= 5,
    OPACITY			= 6,
    MODE			= 7,
    VISIBLE			= 8,
    LINKED			= 9,
    PRESERVE_TRANSPARENCY	= 10,
    APPLY_MASK			= 11,
    EDIT_MASK			= 12,
    SHOW_MASK			= 13,
    SHOW_MASKED			= 14,
    OFFSETS			= 15,
    COLOR			= 16,
    COMPRESSION			= 17,
    GUIDES			= 18,

    SPLINES_START		= 19,
    SPLINES_CONTINUITY		= 20,
    SPLINES_KEYFRAME		= 21,
    SPLINES_ACTIVE		= 22,
    SPLINES_TYPE		= 23,
    SPLINES_DRAW_POINTS		= 24,
    SPLINES_POINTS_START	= 25,
    SPLINES_SAVE_POINT		= 26,
    SPLINES_POINTS_END		= 27,
    SPLINES_END			= 28
  };

  extern const std::string PropType_names[29];

  enum class CompressionType : uint8_t {
    NONE	= 0,
    RLE		= 1,
    ZLIB	= 2,
    FRACTAL	= 3
  };

  extern const std::string CompressionType_names[4];

  enum class ImageType : uint8_t {
    RGB		= 0,
    GREY	= 1,
    INDEXED	= 2
  };

  extern const std::string ImageType_names[3];

  enum class ImageAlpha : uint8_t {
    NONE	= 0,
    NO		= 1,
    YES	        = 2
  };

  extern const std::string ImageAlpha_names[3];

  enum class ImageFormat : uint8_t {
    NONE	= 0,
    RGB		= 1,
    GREY	= 2,
    INDEXED	= 3
  };

  extern const std::string ImageFormat_names[4];

  enum class ImagePrecision : uint8_t {
    NONE	= 0,
    U8		= 1,
    U16		= 2,
    FLOAT	= 3,
    FLOAT16	= 4,
    BFP		= 5
  };

  extern const std::string ImagePrecision_names[6];

  enum class StorageType : uint32_t {
    NONE	= 0,
    FLAT	= 1,
    TILED	= 2,
    SHM		= 3
  };

  extern const std::string StorageType_names[4];

  enum class AutoAlloc : uint32_t {
    NONE	= 0,
    OFF		= 1,
    ON		= 2
  };

  extern const std::string AutoAlloc_names[3];

  extern const std::string Mode_names[17];

}; // namespace XCF
