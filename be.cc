/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "be.hh"
#include <stdint.h>

template <>
uint8_t read_be<uint8_t>(std::istream& is) {
  uint8_t buf;
  is.read(reinterpret_cast<char*>(&buf), 1);
  return buf;
}

template <>
uint16_t read_be<uint16_t>(std::istream& is) {
  uint8_t buf[2];
  is.read(reinterpret_cast<char*>(buf), 2);
  return (static_cast<uint16_t>(buf[0]) << 8)
    | static_cast<uint16_t>(buf[1]);
}

template <>
uint32_t read_be<uint32_t>(std::istream& is) {
  uint8_t buf[4];
  is.read(reinterpret_cast<char*>(buf), 4);
  return (static_cast<uint32_t>(buf[0]) << 24)
    | (static_cast<uint32_t>(buf[1]) << 16)
    | (static_cast<uint32_t>(buf[2]) << 8)
    | static_cast<uint32_t>(buf[3]);
}

template <>
float read_be<float>(std::istream& is) {
  uint32_t be_bytes = read_be<uint32_t>(is);
  float *temp = reinterpret_cast<float*>(&be_bytes);
  return *temp;
}

template <>
uint8_t read_be<uint8_t>(const std::vector<uint8_t>& bytes, uint32_t start) {
  return bytes[start];
}

template <>
int8_t read_be<int8_t>(const std::vector<uint8_t>& bytes, uint32_t start) {
  return static_cast<int8_t>(bytes[start]);
}

template <>
uint32_t read_be<uint32_t>(const std::vector<uint8_t>& bytes, uint32_t start) {
  return (static_cast<uint32_t>(bytes[start]) << 24)
    | (static_cast<uint32_t>(bytes[start + 1]) << 16)
    | (static_cast<uint32_t>(bytes[start + 2]) << 8)
    | static_cast<uint32_t>(bytes[start + 3]);
}

template <>
int32_t read_be<int32_t>(const std::vector<uint8_t>& bytes, uint32_t start) {
  return static_cast<int32_t>(read_be<uint32_t>(bytes, start));
}

template <>
float read_be<float>(const std::vector<uint8_t>& bytes, uint32_t start) {
  uint32_t be_bytes = read_be<uint32_t>(bytes, start);
  float *temp = reinterpret_cast<float*>(&be_bytes);
  return *temp;
}

