/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "xcf.hh"
#include <iostream>
#include <fstream>
#include <chrono>
#include <fmt/core.h>

int main(int argc, char* argv[]) {
  XCF::File::ptr xcf_file;

  if (argc < 2)
    return -1;

  std::string filepath = argv[1];
  std::string basename;

  if (argc < 3) {
    auto slash = filepath.find_last_of('/');
    auto dot = filepath.rfind(".xcf");
    if (dot < 0)
      dot = filepath.length();
    basename = filepath.substr(slash + 1, dot - slash - 1);
  } else
    basename = argv[2];

  std::ifstream ifs(filepath, std::ios::binary);
  if (!ifs.is_open())
    return -1;

  auto mtime = fs::last_write_time(filepath);
  xcf_file = XCF::File::load(ifs);

  {
    fs::path yaml_path = basename;
    yaml_path += ".yaml";
    std::ofstream yaml_file(yaml_path);

    YAML::Emitter yaml;
    yaml << *xcf_file;

    yaml_file << yaml.c_str();
    yaml_file.close();
  }

  for (uint32_t layer_num = 0; layer_num < xcf_file->num_layers(); layer_num++) {
    auto layer = xcf_file->layer(layer_num);

    std::string layer_basename = basename;
    layer_basename += " [L" + std::to_string(xcf_file->num_layers() - layer_num - 1) + "]";

    {
      fs::path filepath = layer_basename;
      filepath += "(" + layer.name() + ").png";
      layer.image()->save_png(filepath, xcf_file, layer, mtime);
    }

    if (layer.has_mask()) {
      fs::path filepath = layer_basename;
      filepath += "{mask}(" + layer.mask_name() + ").png";
      layer.mask_image()->save_png(filepath, xcf_file, layer, mtime);
    }
  }

  for (uint32_t channel_num = 0; channel_num < xcf_file->num_channels(); channel_num++) {
    auto channel = xcf_file->channel(channel_num);

    std::string filepath = basename;
    filepath += " [C" + std::to_string(channel_num) + "](" + channel.name() + ").png";

    channel.image()->save_png(filepath, xcf_file, channel, mtime);
  }

  ifs.close();

  return 0;
}
