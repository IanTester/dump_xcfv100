# CinePaint XCF dumper

This is a program to dump the layers and channels from an XCF file produced
by [CinePaint](https://sourceforge.net/projects/cinepaint/).

This program was created after I went through my old film scans and found
some .xcf files that couldn't be read with [The Gimp](https://www.gimp.org/).
It simply reported "unsupported XCF file version 100 encountered". Some
searching didn't turn up much but I think one page mentioned CinePaint and
then I remembered that I had resorted to using it to edit the film scans
because The Gimp did not support 16-bit components at that time.

This program was written using [an archived copy of CinePaint's XCF
documentation](https://web.archive.org/web/20161024115140/http://www.cinepaint.org/more/docs/xcf.html)
and a copy of the CinePaint 1.3 source I still had on my system.

## Working features
* Saves a YAML file describing all of the metadata in the XCF file
* Saves each layer and any layer mask as a seperate PNG file
* Saves layer/channel properties to a comment block in the PNG file
* 8 and 16-bit component, greyscale and RGB images (with or without alpha)

**NOTE** I only have a few of my own files to test with and they only use
the most basic features.

## Untested features
* Extra "channels"
* Indexed/paletted images

## Unimplemented
* Guides
* Splines

## Requirements
* C++ 20
* [{fmt}](https://github.com/fmtlib/fmt) (because [std::format](https://en.cppreference.com/w/cpp/utility/format)
is not yet implemented in GNU libstc++ or LLVM libc++)
* [yaml-cpp](https://github.com/jbeder/yaml-cpp)
* libpng
