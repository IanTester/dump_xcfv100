/*
        Copyright 2021 Ian Tester

        This file is part of dump_xcfv100.

        dump_xcfv100 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_xcfv100 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_xcfv100.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "be.hh"

namespace XCF {

  template <typename P>
  tag_role<P>::tag_role() :
    _type(0),
    _alpha(0),
    _format(0),
    _precision(0)
  {}

  template <typename P>
  tag_role<P>::tag_role(uint8_t t, uint8_t a, uint8_t f, uint8_t p) :
    _type(t),
    _alpha(a),
    _format(f),
    _precision(p)
  {}


  template <typename P>
  void tag_role<P>::_load_tag(std::istream& is) {
    _precision = read_be<uint8_t>(is);
    _format = read_be<uint8_t>(is);
    _alpha = read_be<uint8_t>(is);
    _type = read_be<uint8_t>(is);
  }

  template <typename P>
  uint32_t tag_role<P>::_num_channels(void) const {
    int num;

    switch (_format) {
    case static_cast<uint8_t>(ImageFormat::RGB):
      num = 3;
      break;

    case static_cast<uint8_t>(ImageFormat::GREY):
    case static_cast<uint8_t>(ImageFormat::INDEXED):
      num = 1;
      break;

    case static_cast<uint8_t>(ImageFormat::NONE):
    default:
      return 0;
    }

    switch (_alpha) {
    case static_cast<uint8_t>(ImageAlpha::YES):
      num++;
      break;

    case static_cast<uint8_t>(ImageAlpha::NO):
      break;

    case static_cast<uint8_t>(ImageAlpha::NONE):
    default:
      return 0;
   }

    return num;
  }

  template <typename P>
  uint32_t tag_role<P>::_bytes_per_channel(void) const {
    switch (_precision) {
    case static_cast<uint8_t>(ImagePrecision::U8):
      return sizeof(uint8_t);

    case static_cast<uint8_t>(ImagePrecision::BFP):
    case static_cast<uint8_t>(ImagePrecision::U16):
      return sizeof(uint16_t);

    case static_cast<uint8_t>(ImagePrecision::FLOAT):
      return sizeof(float);

    case static_cast<uint8_t>(ImagePrecision::FLOAT16):
      return sizeof(uint16_t);

    case static_cast<uint8_t>(ImagePrecision::NONE):
    default:
      return 0;
    }

    return 0;
  }


}; // namespace XCF
